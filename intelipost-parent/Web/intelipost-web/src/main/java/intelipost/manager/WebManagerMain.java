package intelipost.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"intelipost.manager"})
public class WebManagerMain extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		System.setProperty("spring.config.name", "intelipost-config");
		return application.sources(WebManagerMain.class);
	}

	public static void main(String[] args) throws Exception {
		System.setProperty("spring.config.name", "intelipost-config");
		
		SpringApplication.run(WebManagerMain.class, args);
	}
}
