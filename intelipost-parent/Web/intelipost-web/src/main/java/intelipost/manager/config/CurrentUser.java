package intelipost.manager.config;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import intelipost.manager.resource.UserResource;

public class CurrentUser extends User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserResource user;
	
    public CurrentUser(UserResource user) {
        super(user.getEmail(), user.getPassword(), AuthorityUtils.createAuthorityList("ADMIN"));
        this.user = user;
    }
    
    public UserResource getUser() {
        return user;
    }

    public String getId() {
        return user.getId();
    }

    public String getProfile() {
        return "ADMIN";
    }
}
