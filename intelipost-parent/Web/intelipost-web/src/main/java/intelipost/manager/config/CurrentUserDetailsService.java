package intelipost.manager.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import intelipost.manager.repository.UserRepository;
import intelipost.manager.resource.UserResource;

@Service
public class CurrentUserDetailsService implements UserDetailsService  {

	private static final Logger LOG = Logger.getLogger(CurrentUserDetailsService.class);

	
	@Autowired
    private UserRepository userMongoRepository;
	
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		LOG.debug("Authenticating user with email={" + email + "}");

		UserResource auth;		
		try {			
			
			auth = this.userMongoRepository.findByEmail("tanaka@a.com");
			
        } catch (Exception ex) {
            LOG.error("Error in retrieving user");
            throw new UsernameNotFoundException("Was not found user {" + email + "}");
        }

		return new CurrentUser(auth);
	}
	
}
