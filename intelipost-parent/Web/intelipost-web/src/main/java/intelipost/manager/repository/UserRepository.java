package intelipost.manager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import intelipost.manager.resource.UserResource;

public interface UserRepository extends CrudRepository<UserResource, String> {

	public UserResource findByEmail(@Param("email") String email);
	

}
