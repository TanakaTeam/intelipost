<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div class="container">

	<link rel="stylesheet" href="/css/login.css" />
	
	
	<c:if test="${param.error ne null}">
        <div>
            Invalid username and password.
        </div>
     </c:if>
         <c:if test="${param.logout ne null}">
        <div>
            You have been logged out.
        </div>
      </c:if>

	<form class="form-signin" action="<c:url value="/login"/>" method="post">
	
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	
		<label for="input-email" class="sr-only">Email address</label> 
		
		<input type="email" id="input-email" class="form-control"
			name="username"
			placeholder="Email address" required autofocus> 
		
		<label for="input-password" class="sr-only">Password</label>
		<input
			name="password"
			type="password" id="input-password" class="form-control"
			placeholder="Password" required>
			
		<button class="btn btn-lg btn-primary btn-block" type="submit"> Sign
			in</button>
	</form>

</div>