import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import intelipost.manager.repository.UserRepository;
import intelipost.manager.resource.UserResource;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={SpringBootApplication.class})
public class UserTest {
	
	@Autowired
    private UserRepository userMongoRepository;

    @Test
    public void setUp() throws Exception {
        UserResource user= new UserResource();
        
        user.setEmail("tanaka@a.com");
        user.setPassword("$2a$10$ZCDmmh6RnO33l1ZLqJ08wOmwE/ynnxFZ2Xdi1P3ly9MqEJCQvXZeu");     
        
        this.userMongoRepository.save(user);
        assertNotNull(user.getId());
    }
	
}
