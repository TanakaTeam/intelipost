<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>Intelipost</title>
		<link rel="stylesheet" href="/css/bootstrap.min.css" />
	</head>
<body>

	<div id="main">
		<script src="/js/jquery-3.2.1.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>

		<%-- 			<tiles:insertAttribute name="menu_top" /> --%>
		<%-- 			<tiles:insertAttribute name="menu_left" /> --%>


		<%-- 		    	<tiles:insertAttribute name="header" /> --%>



		<tiles:insertAttribute name="body" />



		<%-- 		 	<tiles:insertAttribute name="footer" /> --%>

	

	</div>

</body>
</html>